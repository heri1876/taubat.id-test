package com.shadow.taubatid.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

public class dbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "ustadz.db";
    public static final String TABLE_NAME = "dataUstadz";

    public static final String COLUMN_NAMA = "nama";
    public static final String COLUMN_FOTO = "foto";

    public dbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String CreateTable = "CREATE TABLE " + TABLE_NAME + "(id INT NOT NULL," + COLUMN_NAMA+ " TEXT NOT NULL, "
                + COLUMN_FOTO + " TEXT NOT NULL)";

        db.execSQL(CreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void Input(int id, String nama, String foto){
        SQLiteDatabase database = this.getWritableDatabase();

        String query = "INSERT INTO " + TABLE_NAME + " (id, nama, foto) VALUES (" + id + ",'" + nama + "','" + foto + "')" ;

        database.execSQL(query);
        database.close();
    }

    public void deleteTB() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_NAME);
    }

    public int jumlah() {
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT  count(*) FROM " + TABLE_NAME;

        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.moveToFirst();

        return cursor.getInt(0);
    }

    public HashMap<String, String> getTBData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        HashMap<String, String> dataUstadz = new HashMap<String, String>();

        String query = "select * from "+ TABLE_NAME +" where id=?";
        Cursor cursor = db.rawQuery(query, new String[]{"" + id});

        if (cursor.moveToFirst()) {
            dataUstadz.put("id", cursor.getString(0));
            dataUstadz.put("nama", cursor.getString(1));
            dataUstadz.put("foto", cursor.getString(2));
        }

        return dataUstadz;
    }

}
