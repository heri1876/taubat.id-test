package com.shadow.taubatid.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.shadow.taubatid.R;
import com.shadow.taubatid.data.ustadz;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ListViewAdapter extends ArrayAdapter<ustadz> {
    private List<ustadz> ustadzList;
    Context context;
    int resource;

    public ListViewAdapter(Context context, int resource){
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent){

        View listViewItem = convertView;

        if (listViewItem == null){
            listViewItem = LayoutInflater.from(context).inflate(resource, null);
        }

        TextView nama = listViewItem.findViewById(R.id.nama);
        CircleImageView foto = listViewItem.findViewById(R.id.foto);

        nama.setText(getItem(position).getNama());
        Glide.with(getContext()).load(getItem(position).getFoto()).into(foto);

        return listViewItem;
    }
}
