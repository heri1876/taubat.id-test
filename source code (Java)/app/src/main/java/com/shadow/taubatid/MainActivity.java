package com.shadow.taubatid;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.shadow.taubatid.adapter.ListViewAdapter;
import com.shadow.taubatid.data.ustadz;
import com.shadow.taubatid.db.dbHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ListViewAdapter adapter;

    private dbHelper dbHelper = new dbHelper(this);
    private ArrayList<ustadz> ustadzArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listUstadz);

        adapter = new ListViewAdapter(this, R.layout.list_ustadz);
        listView.setAdapter(adapter);

        getData();
        dynamicSize();

    }

    private void getData(){
        adapter.clear();
        ustadzArrayList.clear();

        for (int i = 0; i < dbHelper.jumlah(); i++){
            HashMap<String, String> ustadz = dbHelper.getTBData(i);

            ustadz data = new ustadz();

            data.setNama(ustadz.get("nama"));
            data.setFoto(ustadz.get("foto"));

            ustadzArrayList.add(data);
        }

        adapter.addAll(ustadzArrayList);
        adapter.notifyDataSetChanged();
    }

    void dynamicSize(){
        int totalHeight = 0;

        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (adapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

}
