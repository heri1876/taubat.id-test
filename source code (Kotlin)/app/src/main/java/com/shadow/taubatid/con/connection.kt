package com.shadow.taubatid.con

import android.content.Context
import android.util.Log
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import com.shadow.taubatid.db.dbHelper
import cz.msebera.android.httpclient.Header
import org.json.JSONObject

class connection() {

    fun reqData(context: Context) {
        val url = "http://private-bba9f-heri1876.apiary-mock.com/Api/tampil_semua_ustadz.php"
        val client = AsyncHttpClient()

        client.get(url, object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int,
                headers: Array<Header>,
                responseBody: ByteArray
            ) {
                Log.d("DEBUG", "Berhasil")
                val dbHelper = dbHelper(context)
                val result = String(responseBody)

                try {
                    val jsonObject = JSONObject(result)

                    for (i in 0 until jsonObject.getJSONArray("result").length()) {
                        val id = jsonObject.getJSONArray("result").getJSONObject(i).getInt("id")
                        val nama =
                            jsonObject.getJSONArray("result").getJSONObject(i).getString("nama")
                        val foto =
                            jsonObject.getJSONArray("result").getJSONObject(i).getString("foto")

                        dbHelper.Input(id, nama, foto)
                    }
                } catch (e: Exception) {

                }
            }

            override fun onFailure(
                statusCode: Int,
                headers: Array<Header>,
                responseBody: ByteArray,
                error: Throwable
            ) {
            }
        })
    }
}