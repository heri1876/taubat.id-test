package com.shadow.taubatid.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import com.bumptech.glide.Glide
import com.shadow.taubatid.R
import com.shadow.taubatid.data.ustadz

import de.hdodenhof.circleimageview.CircleImageView

class ListViewAdapter(internal var context: Context, internal var resource: Int) :
    ArrayAdapter<ustadz>(context, resource) {
    private val ustadzList: List<ustadz>? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var listViewItem = convertView

        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(context).inflate(resource, null)
        }

        val nama: TextView = listViewItem!!.findViewById(R.id.nama)
        val foto: CircleImageView = listViewItem.findViewById(R.id.foto)

        nama.setText(getItem(position)!!.nama)
        Glide.with(getContext()).load(getItem(position)!!.foto).into(foto)

        return listViewItem
    }
}