package com.shadow.taubatid.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import java.util.*

class dbHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        val CreateTable =
            ("CREATE TABLE " + TABLE_NAME + "(id INT NOT NULL," + COLUMN_NAMA + " TEXT NOT NULL, "
                    + COLUMN_FOTO + " TEXT NOT NULL)")

        db.execSQL(CreateTable)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    fun Input(id: Int, nama: String, foto: String) {
        val database = this.writableDatabase

        val query = "INSERT INTO $TABLE_NAME (id, nama, foto) VALUES ($id,'$nama','$foto')"

        database.execSQL(query)
        database.close()
    }

    fun deleteTB() {
        val db = this.writableDatabase
        db.execSQL("delete from $TABLE_NAME")
    }

    fun jumlah(): Int {
        val db = this.readableDatabase
        val countQuery = "SELECT  count(*) FROM $TABLE_NAME"

        val cursor = db.rawQuery(countQuery, null)
        cursor.moveToFirst()

        return cursor.getInt(0)
    }

    fun getTBData(id: Int): HashMap<String, String> {
        val db = this.readableDatabase
        val dataUstadz = HashMap<String, String>()

        val query = "select * from $TABLE_NAME where id=?"
        val cursor = db.rawQuery(query, arrayOf("" + id))

        if (cursor.moveToFirst()) {
            dataUstadz["id"] = cursor.getString(0)
            dataUstadz["nama"] = cursor.getString(1)
            dataUstadz["foto"] = cursor.getString(2)
        }

        return dataUstadz
    }

    companion object {

        private val DATABASE_VERSION = 1

        val DATABASE_NAME = "ustadz.db"
        val TABLE_NAME = "dataUstadz"

        val COLUMN_NAMA = "nama"
        val COLUMN_FOTO = "foto"
    }

}
