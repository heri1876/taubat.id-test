package com.shadow.taubatid

import android.os.Bundle
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.shadow.taubatid.adapter.ListViewAdapter
import com.shadow.taubatid.data.ustadz
import com.shadow.taubatid.db.dbHelper
import java.util.*

class MainActivity : AppCompatActivity() {

    internal lateinit var listView: ListView

    internal lateinit var adapter: ListViewAdapter
    internal var dbHelper = dbHelper(this)
    private val ustadzArrayList = ArrayList<ustadz>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listView = findViewById(R.id.listUstadz)

        adapter = ListViewAdapter(this, R.layout.list_ustadz)
        listView.adapter = adapter

        getData()
        dynamicSize()

    }

    private fun getData() {
        adapter.clear()
        ustadzArrayList.clear()

        for (i in 0 until dbHelper.jumlah()) {
            val ustadz = dbHelper.getTBData(i)

            val data = ustadz()

            data.nama = ustadz.get("nama")
            data.foto = ustadz.get("foto")

            ustadzArrayList.add(data)
        }

        adapter.addAll(ustadzArrayList)
        adapter.notifyDataSetChanged()
    }

    internal fun dynamicSize() {
        var totalHeight = 0

        for (i in 0 until adapter.getCount()) {
            val listItem = adapter.getView(i, null, listView)
            listItem.measure(0, 0)
            totalHeight += listItem.getMeasuredHeight()
        }
        val params = listView.layoutParams
        params.height = totalHeight + listView.dividerHeight * (adapter.getCount() - 1)
        listView.layoutParams = params
    }

}
